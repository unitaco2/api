<?php

namespace Unitaco\Api;

use Unitaco\Api\History\Request;
use Unitaco\Api\History\Answer;

/**
 * Implements history request and answer creating interface.
 */
class History
{
    /**
     * Creates new instance of History\Request
     * 
     * @return Unitaco\Api\History\Request
     */
    public static function request()
    {
        return Request::create();
    }
    
    /**
     * Creates new instance of History\Answer from an received answer
     * 
     * @param Unitaco\Api\History\Answer $data received answer xml string
     * @return Unitaco\Api\History\Answer
     */
    public static function answer($data)
    {
        return Answer::from($data);
    }
    
    /**
     * Gets the history api url to send request.
     * 
     * @return string
     */
    public static function apiUrl()
    {
        return 'https://unitaco.com/api/history';
    }
}