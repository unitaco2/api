<?php

namespace Unitaco\Api\History;

use DOMDocument;
use DOMNodeList;
use Unitaco\Api\History\Record;
use Unitaco\Api\Traits\Utils;

/**
 * Implements history answer basing on received xml string.
 * 
 * Usage:
 * ```
 * $xml = <<<EOD
 * <?xml version="1.0" encoding="utf-8"?>
 * <history>
 *     <result>
 *         <retval>0</retval>
 *         <message>success</message>
 *     </result>
 *     <login>01XXXXX</login>
 *     <date>2011-11-17T18:33:13</date>
 *     <records_count>2</records_count>
 *     <records>
 *         <record>
 *             <date>2011-11-07 14:41:59</date>
 *             <comment>Оплата за услуги со счета #2342</comment>
 *             <status>done</status>
 *             <type>pay</type>
 *             <transaction>987654321</transaction>
 *             <currency>USD</currency>
 *             <result>success</result>
 *             <info>
 *                 <sum>10</sum>
 *                 <comission>0.05</comission>
 *
 *                 <balance>7359.98</balance>
 *                 <order>1233</order>
 *             </info>
 *         </record>
 *         <record>
 *             <date>2011-10-27 18:05:14</date>
 *             <comment>qwerqwe</comment>
 *             <status>done</status>
 *             <type>split</type>
 *             <transaction>876543210</transaction>
 *             <currency>USD</currency>
 *             <result>success</result>
 *             <info>
 *                 <data>
 *                     <issue>
 *                         <number>R45933082KGJ</number>
 *                         <code>WM3FC</code>
 *                         <sum>2</sum>
 *                     </issue>
 *                     <issue>
 *                         <number>R91629655QKQ</number>
 *                         <code>X3JSI</code>
 *                         <sum>3</sum>
 *                     </issue>
 *                 </data>
 *                 <sum>5</sum>
 *                 <comission>1</comission>
 *                 <balance>123.8</balance>
 *             </info>
 *         </record>
 *      </records>
 * </history>
 * EOD;
 * 
 * $answer = Unitaco\Api\History::answer($xml);
 * assert($answer->retval === 0);
 * ```
 */
class Answer
{
    use Utils;
    
    /**
     * Error code
     * @var integer
     */
    public $retval;
    
    /**
     * Result message
     * @var string
     */
    public $message;
    
    /**
     * Agent or merchant login
     * @var string
     */
    public $login;
    
    /**
     * Answer date
     * @var DateTime
     */
    public $date;
    
    /**
     * Found records list
     * @var Record[]
     */
    public $records;
    
    /**
     * Creates new instance of Answer from received answer xml string
     * 
     * @param string $data
     * @return \Unitaco\Api\History\Answer
     */
    public static function from($data)
    {
        $answer = new static;
        
        $dom = new DOMDocument;
        $dom->loadXML($data);
        $answer->retval = (int)$answer->get($dom, 'retval');
        $answer->message = $answer->get($dom, 'message');
        $answer->login = $answer->get($dom, 'login');
        $answer->date = $answer->getDate($dom, 'date');
        
        if ((int)$dom->getElementsByTagName('records_count')->item(0)->textContent > 0) {
            $answer->collectRecords($dom->getElementsByTagName('record'));
        }
        
        return $answer;
    }
    
    protected function collectRecords(DOMNodeList $records)
    {
        $this->records = [];
        
        foreach ($records as $record) {
            $this->records[] = Record::from($record);
        }
    }
}
