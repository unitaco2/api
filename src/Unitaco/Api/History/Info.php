<?php

namespace Unitaco\Api\History;

use DOMElement;
use Unitaco\Api\Traits\Utils;

/**
 * Implements `info` section in history answer's record.
 */
class Info
{
    use Utils;
    
    public $sum;
    public $comission;
    public $balance;
    public $order;
    
    /**
     * Creates new instance of Info from `info` node.
     * 
     * @param DOMElement $node
     * @return \Unitaco\Api\History\Info
     */
    public static function from(DOMElement $node)
    {
        $info = new static;
        
        $info->sum = $info->get($node, 'sum');
        $info->comission = $info->get($node, 'comission');
        $info->balance = $info->get($node, 'balance');
        $info->order = $info->get($node, 'order');
        
        return $info;
    }
    
    protected function __construct(){}
}