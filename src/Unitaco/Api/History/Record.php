<?php

namespace Unitaco\Api\History;

use DOMElement;
use DOMNodeList;
use Unitaco\Api\Traits\Utils;

/**
 * Implements single record from section `records` in history answer.
 */
class Record
{
    use Utils;
    
    /**
     * Record date
     * @var DateTime
     */
    public $date;
    
    /**
     * Record comment
     * @var string
     */
    public $comment;
    
    /**
     * Record status defines if the record is done.
     * @var type 
     */
    public $status;
    
    /**
     * Record type defines if the record is `split` or `pay` type.
     * @var string
     */
    public $type;
    
    /**
     * Record transaction code.
     * @var string
     */
    public $transaction;
    
    /**
     * Record currency.
     * @var string
     */
    public $currency;
    
    /**
     * Record requesting result
     * @var type 
     */
    public $result;
    
    /**
     * Record info
     * @var \Unitaco\Api\History\Info 
     */
    public $info;
    
    /**
     * Record issues list
     * @var \Unitaco\Api\History\Issue[]
     */
    public $issues;

    /**
     * Creates new instance of Record from xml node `record`.
     *  
     * @param DOMElement $node records node
     * @return \Unitaco\Api\History\Record 
     */
    public static function from(DOMElement $node)
    {
        $record = new static;
        
        $record->date = $record->getDate($node, 'date');
        $record->comment = $record->get($node, 'comment');
        $record->status = $record->get($node, 'status');
        $record->type = $record->get($node, 'type');
        $record->transaction = $record->get($node, 'transaction');
        $record->currency = $record->get($node, 'currency');
        $record->result = $record->get($node, 'result');
        
        $info = $node->getElementsByTagName('info');
        if ($info->length > 0) {
            $record->info = Info::from($info->item(0));
        }
        
        $issues = $node->getElementsByTagName('issue');
        if ($issues->length > 0) {
            $record->collectIssues($issues);
        }
        
        return $record;
    }
    
    protected function __construct(){}
    
    protected function collectIssues(DOMNodeList $issues)
    {
        $this->issues = [];
        
        foreach ($issues as $issue) {
            $this->issues[] = Issue::from($issue);
        }
    }
}
