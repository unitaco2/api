<?php

namespace Unitaco\Api\History;

use DOMElement;
use Unitaco\Api\Traits\Utils;

/**
 * Implements single issue from section `issues` of history answer
 */
class Issue
{
    use Utils;
    
    /**
     * Issue number
     * @var string
     */
    public $number;
    
    /**
     * Issue code
     * @var string
     */
    public $code;
    
    /**
     * Issue sum
     * @var float 
     */
    public $sum;
    
    /**
     * Creates new instance of Issue from xml node `issue`.
     * 
     * @param DOMElement $node
     * @return Unitaco\Api\History\Issue
     */
    public static function from(DOMElement $node)
    {
        $issue = new static;
        
        $issue->number = (int)$issue->get($node, 'number');
        $issue->code = $issue->get($node, 'code');
        $issue->sum = (float)$issue->get($node, 'sum');
        
        return $issue;
    }
    
    protected function __construct(){}
}

