<?php

namespace Unitaco\Api\History;

use DateTime;
use DOMDocument;
use LogicException;
use Unitaco\Api\Traits\RequestUser;

/**
 * Constructs history request xml string.
 * 
 * Usage:
 * ```
 * $history = \Unitaco\Api\History::request()
 *      ->login('01XXXXX')
 *      ->password('pwd')
 *      ->from(new DateTime('2015-09-14'))
 *      ->to(new DateTime('2015-09-17'))
 *      ->typeSplit()
 *      ->order('1234')
 *      ->transaction('987654321')
 *      ->build();
 *
 * echo $history;
 * ```
 */
class Request
{
    use RequestUser;
    
    const TYPE_SPLIT = 0;
    const TYPE_PAY = 1;
    
    protected $_login;
    protected $_password;
    protected $_from;
    protected $_to;
    protected $_type;
    protected $_order;
    protected $_transaction;
    
    /**
     * Creates new instance of History\Request
     * 
     * @return \Unitaco\Api\History\Request
     */
    public static function create()
    {
        return new static;
    }
    
    /**
     * Sets start date boundary from which the search will start
     * 
     * @param DateTime $from
     * @return \Unitaco\Api\History\Request
     */
    public function from(DateTime $from)
    {
        $this->_from = $from;
        return $this;
    }
    
    /**
     * Sets end date boundary to which the search will countinue
     * 
     * @param DateTime $to
     * @return \Unitaco\Api\History\Request
     */
    public function to(DateTime $to)
    {
        $this->_to = $to;
        return $this;
    }
    
    /**
     * Sets the request type to `split`. 
     * 
     * @return \Unitaco\Api\History\Request
     */
    public function typeSplit()
    {
        $this->_type = self::TYPE_SPLIT;
        return $this;
    }
    
    /**
     * Sets the request type to `pay`.
     * 
     * @return \Unitaco\Api\History\Request
     */
    public function typePay()
    {
        $this->_type = self::TYPE_PAY;
        return $this;
    }
    
    /**
     * Sets the order ID
     * 
     * @param type $order should contain 10 digits
     * @return \Unitaco\Api\History\Request
     */
    public function order($order)
    {
        $this->_order = $order;
        return $this;
    }
    
    /**
     * Sets the transaction code
     * 
     * @param type $transaction
     * @return \Unitaco\Api\History\Request
     */
    public function transaction($transaction)
    {
        $this->_transaction = $transaction;
        return $this;
    }
    
    /**
     * Builds request xml string 
     * 
     * @return string
     */
    public function build()
    {
        $dom = new DOMDocument('1.0', 'utf8');
        $request = $dom->appendChild($dom->createElement('request'));
        $request->appendChild($this->buildUser($dom));
        
        if ($this->_from !== null) {
            $request->appendChild($this->buildDate($dom, 'from', $this->_from));
        }
        
        if ($this->_to !== null) {
            $request->appendChild($this->buildDate($dom, 'to', $this->_to));
        }
        
        if ($this->_type !== null) {
            $request->appendChild($this->buildElement($dom, 'type', $this->getTypeName()));
        }
        
        if ($this->_order !== null) {
            $request->appendChild($this->buildElement($dom, 'order', $this->_order));
        }
        
        if ($this->_transaction) {
            $request->appendChild($this->buildElement($dom, 'transaction', $this->_transaction));
        }
        
        return $dom->saveXML();
    }
    
    protected function __construct() {}
    
    protected function buildDate(DOMDocument $dom, $name, DateTime $value)
    {
        $date = $dom->createElement($name);
        $date->appendChild($dom->createTextNode($value->format('Y-m-d\TH:i:s')));
        
        return $date;
    }
    
    protected function buildElement(DOMDocument $dom, $name, $value)
    {
        $element = $dom->createElement($name);
        $element->appendChild($dom->createTextNode($value));
        
        return $element;
    }
    
    protected function getTypeName()
    {
        switch ($this->_type) {
            case self::TYPE_SPLIT:
                return 'split';
            case self::TYPE_PAY:
                return 'pay';
            default:
                throw new LogicException('Type is wrong');
        }
    }
}