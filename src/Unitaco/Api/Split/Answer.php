<?php

namespace Unitaco\Api\Split;

use DOMDocument;
use Unitaco\Api\Traits\Utils;

/**
 * Implements split answer basing on received xml string.
 * 
 * Usage:
 * ```
 * $xml = <<<EOD
 * <?xml version="1.0" encoding="utf-8"?>
 * <split>
 *     <result>
 *         <retval>0</retval>
 *         <message>success</message>
 *     </result>
 *     <login>01XXXX</login>
 *     <date>2011-11-17T18:33:13</date>
 *     <transaction>987654321</transaction>
 * </split>
 * EOD;
 * 
 * $answer = \Unitaco\Api\Split::answer($xml);
 * assert($answer->retval === 0);
 */
class Answer
{
    use Utils;
    
    /**
     * Error code
     * @var integer 
     */
    public $retval;
    
    /**
     * Result message
     * @var string
     */
    public $message;
    
    /**
     * Agent or merchant login
     * @var string
     */
    public $login;
    
    /**
     * Answer date
     * @var DateTime
     */
    public $date;
    
    /**
     * Transaction code
     * @var string
     */
    public $transaction;
    
    /**
     * Creates new instance of Split\Answer from received answer xml string
     * 
     * @param string $data
     * @return \Unitaco\Api\Split\Answer
     */
    public static function from($data)
    {
        $answer = new static;
        
        $dom = new DOMDocument;
        $dom->loadXML($data);
        $answer->retval = (int)$answer->get($dom, 'retval');
        $answer->message = $answer->get($dom, 'message');
        $answer->login = $answer->get($dom, 'login');
        $answer->date = $answer->getDate($dom, 'date');
        $answer->transaction = $answer->get($dom, 'transaction');
        
        return $answer;
    }
}
