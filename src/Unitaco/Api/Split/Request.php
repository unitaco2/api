<?php

namespace Unitaco\Api\Split;

use DOMDocument;
use LogicException;
use Unitaco\Api\Traits\RequestUser;

/**
 * Constructs split request xml string
 * 
 * Usage:
 * ```
 * $request = \Unitaco\Api\Split::request()
 *     ->login('01XXXX')
 *     ->password('pwd')
 *     ->sums([2, 5])
 *     ->comment('Some agent operation comment')
 *     ->build();
 * 
 * echo $request;
 * ```
 */
class Request
{
    use RequestUser;
    
    const TYPE_SPLIT = 0;
    const TYPE_PAY = 1;
    
    protected $_login;
    protected $_password;
    protected $_sums;
    protected $_comment;
    
    /**
     * Creates new instance of History\Request
     * 
     * @return \Unitaco\Api\History\Request
     */
    public static function create()
    {
        return new static;
    }
    
    /**
     * Sets list of requesting sums
     * 
     * @param float[] $sums
     * @return \Unitaco\Api\Split\Request
     */
    public function sums(array $sums)
    {
        $this->_sums = $sums;
        return $this;
    }
    
    /**
     * Sets request comment
     * 
     * @param string $comment
     * @return \Unitaco\Api\Split\Request
     */
    public function comment($comment)
    {
        $this->_comment = $comment;
        return $this;
    }
    
    /**
     * Builds request xml string
     * 
     * @return string
     */
    public function build()
    {
        $dom = new DOMDocument('1.0', 'utf8');
        $request = $dom->appendChild($dom->createElement('request'));
        $request->appendChild($this->buildUser($dom));
        
        if ($this->_sums !== null) {
            $request->appendChild($this->buildSums($dom));
        }
        
        if ($this->_comment !== null) {
            $request->appendChild($this->buildElement($dom, 'comment', $this->_comment));
        }
        
        return $dom->saveXML();
    }
    
    protected function __construct() {}
    
    protected function buildSums(DOMDocument $dom)
    {
        $sums = $dom->createElement('sums');
        
        foreach ($this->_sums as $sum) {
            $sums->appendChild($this->buildElement($dom, 'sum', $sum));
        }

        return $sums;
    }
    
    protected function buildElement(DOMDocument $dom, $name, $value)
    {
        $element = $dom->createElement($name);
        $element->appendChild($dom->createTextNode($value));
        
        return $element;
    }

    protected function getTypeName()
    {
        switch ($this->_type) {
            case self::TYPE_SPLIT:
                return 'split';
            case self::TYPE_PAY:
                return 'pay';
            default:
                throw new LogicException('Type is wrong');
        }
    }
}