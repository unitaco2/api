<?php

namespace Unitaco\Api;

use Unitaco\Api\Split\Request;
use Unitaco\Api\Split\Answer;

/**
 * Implements split request and answer creating interface.
 */
class Split
{
    /**
     * Creates new instance of Split\Request
     * 
     * @return Unitaco\Api\Split\Request
     */
    public static function request()
    {
        return Request::create();
    }
    
    /**
     * Creates new instance of Split\Answer from an received answer
     * 
     * @param Unitaco\Api\Split\Answer $data received answer xml string
     * @return Unitaco\Api\Split\Answer
     */
    public static function answer($data)
    {
        return Answer::from($data);
    }
    
    /**
     * Gets the split api url to send request.
     * 
     * @return string
     */
    public static function apiUrl()
    {
        return 'https://unitaco.com/api/split';
    }
}