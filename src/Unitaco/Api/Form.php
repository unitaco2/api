<?php

namespace Unitaco\Api;

use LogicException;

/**
 * Constructs the simple form with one button to perform predefined payment
 * to single merchant.
 * 
 * Usage:
 * ```
 * $form = \Unitaco\Api\Form::create()
 *    ->merchant('01XXXXX')
 *    ->currency(\Unitaco\Api\Form::CURRENCY_RUR)
 *    ->order('NP0001')
 *    ->comment('Оплата за услуги со счета #0001')
 *    ->total(10)
 *    ->methodPost()
 *    ->backUrl('https://www.example.com/')
 *    ->button('Отправить')
 *    ->build();
 * 
 * echo $form;
 * ```
 */
class Form
{
    const METHOD_POST = 0;
    const METHOD_GET = 1;
    
    const CURRENCY_RUR = 0;
    const CURRENCY_USD = 1;
    const CURRENCY_EUR = 2;
    
    protected $_merchant;
    protected $_currency = self::CURRENCY_RUR;
    protected $_order; 
    protected $_comment;
    protected $_total;
    protected $_method;
    protected $_url;
    protected $_buttonName = 'Send';
    
    protected $_apiUrl = 'https://unitaco.com/transfer/';
    
    /**
     * Creates new instance of Form.
     * 
     * @return Unitaco\Api\Form
     */
    public static function create()
    {
        return new static;
    }
    
    /**
     * Sets merchant ID.
     * 
     * @param string $merchant merchant ID
     * @return \Unitaco\Api\Form
     */
    public function merchant($merchant)
    {
        $this->_merchant = $merchant;
        return $this;
    }
    
    /**
     * Sets currency.
     * 
     * @param integer $currency should be one of self::CURRENCY_*
     * @return \Unitaco\Api\Form
     */
    public function currency($currency)
    {
        $this->_currency = $currency;
        return $this;
    }
    
    /**
     * Sets unique order ID guaranteeing that one bill will not be payed twice.
     * 
     * @param string $order should have 10 digits
     * @return \Unitaco\Api\Form
     */
    public function order($order)
    {
        $this->_order = $order;
        return $this;
    }
    
    /**
     * Sets comment for payment.
     * 
     * @param string $comment
     * @return \Unitaco\Api\Form
     */
    public function comment($comment)
    {
        $this->_comment = $comment;
        return $this;
    }
    
    /**
     * Sets payment total sum.
     * 
     * @param integer $total
     * @return \Unitaco\Api\Form
     */
    public function total($total)
    {
        $this->_total = $total;
        return $this;
    }
    
    /**
     * Sets method `POST` for getting answer after the payment performing.
     * 
     * @return \Unitaco\Api\Form
     */
    public function methodPost()
    {
        $this->_method = self::METHOD_POST;
        return $this;
    }
    
    /**
     * Sets method `GET` for getting answer after the payment performing.
     * 
     * @return \Unitaco\Api\Form
     */
    public function methodGet()
    {
        $this->_method = self::METHOD_GET;
        return $this;
    }
    
    /**
     * Sets url to where user will be directed after the payment performing.
     * 
     * @param string $url
     * @return \Unitaco\Api\Form
     */
    public function backUrl($url)
    {
        $this->_url = $url;
        return $this;
    }
    
    /**
     * Sets button text showing for user
     * 
     * @param string $name
     * @return \Unitaco\Api\Form
     */
    public function button($name)
    {
        $this->_buttonName = $name;
        return $this;
    }
    
    /**
     * Builds form html string.
     * 
     * @return string
     */
    public function build()
    {
        $this->check();
        
        $dom = new \DOMDocument();
        $form = $dom->appendChild($this->buildFormTag($dom));
        $form->appendChild($this->buildHidden($dom, 'NP_MERCHANT', $this->_merchant));
        $form->appendChild($this->buildHidden($dom, 'NP_CURRENCY', $this->getCurrencyName()));
        $form->appendChild($this->buildHidden($dom, 'NP_ORDER', $this->_order));
        
        if ($this->_comment) {
            $form->appendChild($this->buildHidden($dom, 'NP_COMMENT', $this->_comment));
        }
        
        $form->appendChild($this->buildHidden($dom, 'NP_TOTAL', $this->_total));
        
        $form->appendChild($this->buildHidden($dom, 'NP_METHOD', $this->getMethodName()));
        $form->appendChild($this->buildHidden($dom, 'NP_URL', $this->_url));
        $form->appendChild($this->buildSubmit($dom));
        
        return $dom->saveHTML();
    }
    
    protected function __construct(){}
    
    protected function check()
    {
        if (!$this->_merchant) {
            throw new LogicException('Merchant should be set');
        }
        
        if (!$this->_total) {
            throw new LogicException('Total sum should be set');
        }
        
        if (!$this->_order) {
            throw new LogicException('Order code should be set');
        }
        
        if (!$this->checkCurrency($this->_currency)) {
            throw new LogicException('Currency is invalid');
        }
    }
    
    protected function checkCurrency($currency)
    {
        switch ($currency) {
            case self::CURRENCY_RUR:
            case self::CURRENCY_USD:
            case self::CURRENCTY_EUR:
                return true;
            default:
                return false;
        }
    }
    
    protected function buildFormTag($dom)
    {
        $form = $dom->createElement('form');
        $form->setAttribute('action', $this->_apiUrl);
        $form->setAttribute('target', '_blank');
        $form->setAttribute('method', 'POST');
        return $form;
    }
    
    protected function buildHidden($dom, $name, $value)
    {
        $input = $dom->createElement('input');
        $input->setAttribute('type', 'hidden');
        $input->setAttribute('name', $name);
        $input->setAttribute('value', $value);
        return $input;
    }
    
    protected function buildSubmit($dom)
    {
        $button = $dom->createElement('button');
        $button->setAttribute('type', 'submit');
        $button->appendChild($dom->createTextNode($this->_buttonName));
        return $button;
    }
    
    protected function getCurrencyName()
    {
        switch ($this->_currency) {
            case self::CURRENCY_RUR:
                return 'RUR';
            case self::CURRENCY_USD:
                return 'USD';
            case self::CURRENCY_EUR:
                return 'EUR';
            default:
                throw new LogicException('Currency is wrong');
        }
    }
    
    protected function getMethodName()
    {
        switch ($this->_method) {
            case self::METHOD_POST:
                return 'POST';
            case self::METHOD_GET:
                return 'GET';
            default:
                throw new LogicException('Method is wrong');
        }
    }
}