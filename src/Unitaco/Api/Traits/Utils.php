<?php

namespace Unitaco\Api\Traits;

use DateTime;

trait Utils
{
    protected function get($node, $name)
    {
        $element = $node->getElementsByTagName($name);
        
        if ($element->length === 0) {
            return '';
        }
        
        return $element->item(0)->textContent;
    }
    
    protected function getDate($node, $name)
    {
        $date = $this->get($node, $name);
        
        if (empty($date)) {
            return;
        }
        
        return new DateTime($date);
    }
}