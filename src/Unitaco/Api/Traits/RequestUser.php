<?php

namespace Unitaco\Api\Traits;

use DOMDocument;

trait RequestUser
{
    /**
     * Sets requesting user login
     * 
     * @param string $login
     * @return static
     */
    public function login($login)
    {
        $this->_login = $login;
        return $this;
    }
    
    /**
     * Sets requesting user password
     * 
     * @param string $password
     * @return static
     */
    public function password($password)
    {
        $this->_password = $password;
        return $this;
    }
    
    protected function buildUser(DOMDocument $dom)
    {
        if ($this->_login === null || $this->_password === null) {
            throw new LogicException('Login and password should be set');
        }
        
        $user = $dom->createElement('user');
        $login = $user->appendChild($dom->createElement('login'));
        $password = $user->appendChild($dom->createElement('password'));
        
        $login->appendChild($dom->createTextNode($this->_login));
        $password->appendChild($dom->createTextNode($this->_password));
        
        return $user;
    }
}